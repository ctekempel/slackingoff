﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using Sitecore.Services.Infrastructure.Web.Http;
using SlackingOff.WorkflowController.Services;

namespace SlackingOff.WorkflowController.Controllers
{
    public class WorkflowController : ServicesApiController
    {
        private readonly WorkflowService _workflowService;

        public WorkflowController()
        {
            _workflowService = new WorkflowService();
        }
        [HttpGet]
        [Route("service/workflow/TriggerWorkflowCommand")]
        public void TriggerWorkflowCommand(Guid itemId, string workflowCommandName, string comment)
        {
            _workflowService.TriggerWorkflowCommand(itemId, workflowCommandName, comment);
        }


        [HttpGet]
        [Route("service/workflow/GetWorkflow")]
        public string GetWorkflow(Guid itemId)
        {
            return _workflowService.GetWorkflow(itemId);
        }

        [HttpGet]
        [Route("service/workflow/GetAvailableWorkflowCommands")]
        public IEnumerable<string> GetAvailableWorkflowCommands(Guid itemId)
        {
            return _workflowService.GetAvailableWorkflowCommands(itemId);
        }

        [HttpGet]
        [Route("service/workflow/GetItemsInWorkflowState")]
        public IEnumerable<Guid> GetItemsInWorkflowState(Guid workflowId, Guid workflowStateId)
        {
            return _workflowService.GetItemsInWorkflowState(workflowId, workflowStateId);
        }
    }
}