﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.SecurityModel;
using Sitecore.Workflows;

namespace SlackingOff.WorkflowController.Services
{
    public class WorkflowService
    {
        private static Database MasterDatabase
        {
            get
            {
                var database = Database.GetDatabase("master");
                return database;
            }
        }

        public void TriggerWorkflowCommand(Guid itemId, string workflowCommandName, string comment)
        {
            using (new SecurityDisabler())
            {
                var item = GetItem(itemId);
                var workflow = GetWorkflow(item);

                var workflowCommands = GetAvailableWorkflowCommands(workflow, item);

                var workflowCommand = workflowCommands.First(command =>
                    command.DisplayName.Equals(workflowCommandName, StringComparison.InvariantCultureIgnoreCase));

                workflow.Execute(workflowCommand.CommandID, item, comment, false);
            }
        }


        public string GetWorkflow(Guid itemId)
        {
            using (new SecurityDisabler())
            {
                var item = GetItem(itemId);
                var workflow = GetWorkflow(item);

                return MasterDatabase.GetItem(new ID(workflow.WorkflowID)).Name;
            }
        }

        public IEnumerable<string> GetAvailableWorkflowCommands(Guid itemId)
        {
            using (new SecurityDisabler())
            {
                var item = GetItem(itemId);
                var workflow = GetWorkflow(item);

                var workflowCommands = GetAvailableWorkflowCommands(workflow, item);

                return workflowCommands.Select(command => command.DisplayName);
            }
        }

        public IEnumerable<Guid> GetItemsInWorkflowState(Guid workflowId, Guid workflowStateId)
        {
            using (new SecurityDisabler())
            {
                var workflowIdString = workflowId.ToString("B").ToUpper();
                var workflowStateIdString = workflowStateId.ToString("B").ToUpper();

                var workflows = MasterDatabase.WorkflowProvider.GetWorkflows();
                workflows.Should().NotBeEmpty("Could not find any workflows");

                var monitoredWorkflow = workflows.FirstOrDefault(workflow => workflow.WorkflowID == workflowIdString);
                monitoredWorkflow.Should().NotBeNull("Could not find specified workflow");

                var itemUrisInState = monitoredWorkflow.GetItems(workflowStateIdString);
                return itemUrisInState.Select(uri => uri.ItemID.Guid);
            }
        }

        private static WorkflowCommand[] GetAvailableWorkflowCommands(IWorkflow workflow, Item item)
        {
            var workflowCommands = workflow.GetCommands(item[FieldIDs.WorkflowState]);
            workflowCommands.Should().NotBeEmpty("Could not find any workflow commands for item");

            return workflowCommands;
        }

        private static Item GetItem(Guid itemId)
        {
            var item = MasterDatabase.GetItem(new ID(itemId));
            item.Should().NotBeNull("Item could not be found");

            return item;
        }

        private static IWorkflow GetWorkflow(Item item)
        {
            var workflowProvider = MasterDatabase.WorkflowProvider;
            var workflow = workflowProvider.GetWorkflow(item);
            workflow.Should().NotBeNull("Could not find workflow for item");

            return workflow;
        }

    }
}