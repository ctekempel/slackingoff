﻿using Sitecore.DataExchange.Attributes;
using Sitecore.DataExchange.Converters.PipelineSteps;
using Sitecore.DataExchange.Models;
using Sitecore.DataExchange.Plugins;
using Sitecore.DataExchange.Repositories;
using Sitecore.Services.Core.Model;

namespace SlackingOff.SlackProvider
{
    [SupportedIds(PostMessageToSlackTemplateId)]
    public class PostMessageToSlackStepConverter : BasePipelineStepConverter
    {
        public const string PostMessageToSlackTemplateId = "{4460B5C4-A105-47DC-8E8F-55A0FA4C4899}";
        public const string TemplateFieldEndpointTo = "EndpointTo";
        public const string FieldNameItemLocation = "ItemLocation";
        public PostMessageToSlackStepConverter(IItemModelRepository repository) : base(repository)
        {
        }
        protected override void AddPlugins(ItemModel source, PipelineStep pipelineStep)
        {
            AddDataLocationSettings(source, pipelineStep);
            AddEndpointSettings(source, pipelineStep);
        }

        private void AddEndpointSettings(ItemModel source, PipelineStep pipelineStep)
        {
            var settings = new EndpointSettings
            {
                EndpointTo = this.ConvertReferenceToModel<Endpoint>(source, TemplateFieldEndpointTo)
            };

            pipelineStep.AddPlugin(settings);
        }

        private void AddDataLocationSettings(ItemModel source, PipelineStep pipelineStep)
        {

            var settings = new DataLocationSettings
            {
                DataLocation = this.GetGuidValue(source, FieldNameItemLocation)
            };
            pipelineStep.AddPlugin<DataLocationSettings>(settings);
        }

    }
}