﻿using Sitecore.DataExchange.Attributes;
using Sitecore.DataExchange.Converters.Endpoints;
using Sitecore.DataExchange.Models;
using Sitecore.DataExchange.Repositories;
using Sitecore.Services.Core.Model;

namespace SlackingOff.SlackProvider
{
    [SupportedIds(SlackEndpointTemplateId)]
    public class SlackEndpointConverter : BaseEndpointConverter
    {
        public const string SlackEndpointTemplateId = "{0E88A23E-1706-4A45-8F6E-5B6C1152D211}";

        public SlackEndpointConverter(IItemModelRepository repository) : base(repository)
        {
        }

        protected override void AddPlugins(ItemModel source, Endpoint endpoint)
        {
            var settings = new SlackProviderSettings
            {
                OAuthToken = GetStringValue(source, OAuthTokenFieldName),
                ChannelName = GetStringValue(source, ChannelNameFieldName),
                MessageTemplate = GetStringValue(source, MessageTemplateFieldName)
            };
            endpoint.AddPlugin(settings);
        }

        #region FieldNames

        public const string OAuthTokenFieldName = "OAuthToken";
        public const string ChannelNameFieldName = "ChannelName";
        public const string MessageTemplateFieldName = "MessageTemplate";

        #endregion
    }
}