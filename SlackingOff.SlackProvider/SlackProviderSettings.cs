﻿using Sitecore.DataExchange;

namespace SlackingOff.SlackProvider
{
    public class 
        SlackProviderSettings : IPlugin
    {
        public string OAuthToken { get; set; }
        public string ChannelName { get; set; }
        public string MessageTemplate { get; set; }
    }
}