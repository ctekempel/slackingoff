﻿using System;
using FluentAssertions;
using Sitecore.Data;
using Sitecore.DataExchange.Attributes;
using Sitecore.DataExchange.Contexts;
using Sitecore.DataExchange.Extensions;
using Sitecore.DataExchange.Models;
using Sitecore.DataExchange.Processors.PipelineSteps;
using Sitecore.Services.Core.Diagnostics;
using SlackNet;
using SlackNet.WebApi;

namespace SlackingOff.SlackProvider
{
    [RequiredEndpointPlugins(typeof(SlackProviderSettings))]
    public class PostMessageToSlackStepProcessor : BasePipelineStepWithEndpointsProcessor
    {
        protected override void ProcessPipelineStep(PipelineStep pipelineStep = null,
            PipelineContext pipelineContext = null,
            ILogger logger = null)
        {
            var endpointSettings = pipelineStep.GetEndpointSettings();
            endpointSettings.Should().NotBeNull("PostMessageToSlackStepProcessor: Endpoint settings should not be null");

            var endpointTo = endpointSettings.EndpointTo;
            endpointTo.Should().NotBeNull("PostMessageToSlackStepProcessor: EndpointTo should not be null");

            var slackProviderSettings = endpointTo.GetSlackProviderSettings();
            slackProviderSettings.Should().NotBeNull("PostMessageToSlackStepProcessor: Slack provider settings should not be null");

            var itemId = GetObjectFromPipelineContext(pipelineStep.GetDataLocationSettings().DataLocation,
                pipelineContext, logger) as Guid?;
            itemId.Should().NotBeNull("PostMessageToSlackStepProcessor: ItemId should not be null");

            var client = new SlackApiClient(slackProviderSettings.OAuthToken);


            var message = slackProviderSettings.MessageTemplate.Replace("{ItemID}", itemId.ToString());
            var result = client.Chat.PostMessage(new Message
                {Channel = slackProviderSettings.ChannelName, Text = message}).Result;
        }
    }
}