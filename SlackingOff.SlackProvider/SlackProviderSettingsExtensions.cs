﻿using Sitecore.DataExchange.Models;

namespace SlackingOff.SlackProvider
{
    public static class SlackProviderSettingsExtensions
    {
        public static SlackProviderSettings GetSlackProviderSettings(this Endpoint endpoint)
        {
            return endpoint.GetPlugin<SlackProviderSettings>();
        }
        public static bool HasSlackProviderSettings(this Endpoint endpoint)
        {
            return (GetSlackProviderSettings(endpoint) != null);
        }
    }
}