﻿using Sitecore.DataExchange;
using Sitecore.DataExchange.Converters;
using Sitecore.DataExchange.DataAccess;
using Sitecore.DataExchange.Repositories;
using Sitecore.Services.Core.Diagnostics;
using Sitecore.Services.Core.Model;

namespace SlackingOff.SlackProvider.General
{
    public class RegexValueReaderConverter : BaseItemModelConverter<IValueReader>
    {
        private const string FieldRegexPattern = "RegexPattern";
        private const string FieldGroupName = "GroupName";

        public RegexValueReaderConverter(IItemModelRepository repository, ILogger logger) : base(repository, logger)
        {
        }

        public RegexValueReaderConverter(IItemModelRepository repository) : base(repository)
        {
        }

        protected override ConvertResult<IValueReader> ConvertSupportedItem(ItemModel source)
        {
            var regexPattern = GetStringValue(source, FieldRegexPattern);
            var groupName = GetStringValue(source, FieldGroupName);
            return ConvertResult<IValueReader>.PositiveResult(new RegexValueReader(regexPattern,groupName));
        }
    }
}