﻿using System;
using System.Text.RegularExpressions;
using Sitecore.DataExchange.DataAccess;

namespace SlackingOff.SlackProvider.General
{
    public class RegexValueReader : IValueReader
    {
        private readonly string _regexPattern;
        private readonly string _groupName;

        public RegexValueReader(string regexPattern, string groupName)
        {
            _regexPattern = regexPattern;
            _groupName = groupName;
        }

        public ReadResult Read(object source, DataAccessContext context)
        {
            if (!(source is string)) return ReadResult.NegativeResult(DateTime.Now);

            var match = Regex.Match((string)source, _regexPattern);
            foreach (Group group in match.Groups)
            {
                if (group.Name == _groupName)
                {
                    return ReadResult.PositiveResult(group.Value, DateTime.Now);
                }
            }
            return ReadResult.NegativeResult(DateTime.Now);
        }
    }
}