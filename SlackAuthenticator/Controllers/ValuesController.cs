﻿using Microsoft.AspNetCore.Mvc;

namespace SlackAuthenticator.Controllers
{
    [Route("{*url}")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        [HttpPost]
        public ActionResult<string> Index(ChallengeRequestModel model)
        {
            return model.challenge;
        }
    }

    public class ChallengeRequestModel
    {
        public string token { get; set; }
        public string challenge { get; set; }
        public string type { get; set; }
    }
}