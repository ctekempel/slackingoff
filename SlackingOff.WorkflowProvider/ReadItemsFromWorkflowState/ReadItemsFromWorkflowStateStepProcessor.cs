﻿using System;
using FluentAssertions;
using Sitecore.DataExchange.Attributes;
using Sitecore.DataExchange.Contexts;
using Sitecore.DataExchange.Extensions;
using Sitecore.DataExchange.Models;
using Sitecore.DataExchange.Plugins;
using Sitecore.DataExchange.Processors.PipelineSteps;
using Sitecore.Services.Core.Diagnostics;
using SlackingOff.WorkflowProvider.Endpoint;
using SlackingOff.WorkflowProvider.Services;

namespace SlackingOff.WorkflowProvider.ReadItemsFromWorkflowState
{
    [RequiredEndpointPlugins(typeof(WorkflowProviderSettings))]
    public class ReadItemsFromWorkflowStateStepProcessor : BasePipelineStepWithEndpointsProcessor
    {
        protected override void ProcessPipelineStep(PipelineStep pipelineStep = null,
            PipelineContext pipelineContext = null,
            ILogger logger = null)
        {
            var endpointSettings = pipelineStep.GetEndpointSettings();
            endpointSettings.Should().NotBeNull("ReadItemsFromWorkflowStateStepProcessor: Endpoint settings should not be null");

            var endpointFrom = endpointSettings.EndpointFrom;
            endpointFrom.Should().NotBeNull("ReadItemsFromWorkflowStateStepProcessor: Endpoint from should not be null");

            var workflowProviderSettings = endpointFrom.GetWorkflowProviderSettings();
            workflowProviderSettings.Should().NotBeNull("ReadItemsFromWorkflowStateStepProcessor: Workflow provider settings should not be null");

            var workflowService = new WorkflowServiceWrapper();
            var itemIds = workflowService.GetItemsInWorkflowState(new Guid(workflowProviderSettings.WorkflowToMonitor),
                new Guid(workflowProviderSettings.WorkflowStateToMonitor));

            var dataSettings = new IterableDataSettings(itemIds);
            pipelineContext.AddPlugin(dataSettings);
        }
    }
}