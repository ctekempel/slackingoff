﻿using Sitecore.DataExchange.Attributes;
using Sitecore.DataExchange.Converters.PipelineSteps;
using Sitecore.DataExchange.Models;
using Sitecore.DataExchange.Plugins;
using Sitecore.DataExchange.Repositories;
using Sitecore.Services.Core.Model;

namespace SlackingOff.WorkflowProvider.ReadItemsFromWorkflowState
{
    [SupportedIds(ReadItemsFromWorkflowStateTemplateId)]
    public class ReadItemsFromWorkflowStateStepConverter : BasePipelineStepConverter
    {
        public const string ReadItemsFromWorkflowStateTemplateId = "{8BF3A81D-0DE2-4455-86A9-5F320D22E44A}";
        public const string TemplateFieldEndpointFrom = "EndpointFrom";

        public ReadItemsFromWorkflowStateStepConverter(IItemModelRepository repository) : base(repository)
        {
        }
        protected override void AddPlugins(ItemModel source, PipelineStep pipelineStep)
        {
            AddEndpointSettings(source, pipelineStep);
        }

        private void AddEndpointSettings(ItemModel source, PipelineStep pipelineStep)
        {
            var settings = new EndpointSettings
            {
                EndpointFrom = this.ConvertReferenceToModel<Sitecore.DataExchange.Models.Endpoint>(source, TemplateFieldEndpointFrom)
            };

            pipelineStep.AddPlugin(settings);
        }


    }
}