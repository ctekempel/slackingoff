﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Sitecore.StringExtensions;
using SlackingOff.WorkflowProvider.TriggerWorkflowCommand.Model;

namespace SlackingOff.WorkflowProvider.Services
{
    public class WorkflowServiceWrapper : IWorkflowService
    {
        private readonly IWorkflowService _workflowService;

        public WorkflowServiceWrapper()
        {
            var sitecoreConnectionString = ConfigurationManager.ConnectionStrings["sitecore"];
            if (sitecoreConnectionString != null && !string.IsNullOrEmpty(sitecoreConnectionString.ConnectionString))
            {
                var connectionStringSegments =
                    sitecoreConnectionString.ConnectionString.Split(';');
                var hostSegment = connectionStringSegments.First(s => s.StartsWith("host="));
                var hostName = hostSegment.Split('=')[1];

                _workflowService = new RemoteWorkflowService(hostName);
            }
            else
            {
                _workflowService = new LocalWorkflowService();

            }
        }

        public void TriggerWorkflowCommand(ExecuteWorkflowCommandRequest commandRequest)
        {
            _workflowService.TriggerWorkflowCommand(commandRequest);
        }

        public IEnumerable<Guid> GetItemsInWorkflowState(Guid workflowId, Guid workflowStateId)
        {
            return _workflowService.GetItemsInWorkflowState(workflowId, workflowStateId);
        }
    }
}