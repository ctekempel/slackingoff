﻿using System;
using System.Collections.Generic;
using System.Net;
using SlackingOff.WorkflowProvider.TriggerWorkflowCommand.Model;

namespace SlackingOff.WorkflowProvider.Services
{
    internal class RemoteWorkflowService : IWorkflowService
    {
        private readonly string _host;

        public RemoteWorkflowService(string host)
        {
            this._host = host;
        }

        public void TriggerWorkflowCommand(ExecuteWorkflowCommandRequest commandRequest)
        {
            using (var webClient = new WebClient())
            {
                var uri =
                    $"http://{_host}/service/workflow/TriggerWorkflowCommand?itemId={commandRequest.ItemId}&workflowCommandName={commandRequest.CommandName}&comment={commandRequest.Comment}";
                var resultString = webClient.DownloadString(uri);
            }
        }

        public IEnumerable<Guid> GetItemsInWorkflowState(Guid workflowId, Guid workflowStateId)
        {
            throw new NotImplementedException();
            //using (var webClient = new WebClient())
            //{
            //    var uri =
            //        $"{_host}/service/workflow/GetItemsInWorkflowState?workflowId={workflowId}&workflowStateId={workflowStateId}";
            //    var resultString = webClient.DownloadString(uri);
            //}
        }
    }
}