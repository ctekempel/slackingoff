﻿using System;
using System.Collections.Generic;
using SlackingOff.WorkflowController.Services;
using SlackingOff.WorkflowProvider.TriggerWorkflowCommand.Model;

namespace SlackingOff.WorkflowProvider.Services
{
    class LocalWorkflowService : IWorkflowService
    {
        private readonly WorkflowService _workflowService;

        public LocalWorkflowService()
        {
            _workflowService = new WorkflowService();
        }
        public void TriggerWorkflowCommand(ExecuteWorkflowCommandRequest commandRequest)
        {
            _workflowService.TriggerWorkflowCommand(new Guid(commandRequest.ItemId),commandRequest.CommandName, commandRequest.Comment);
        }

        public IEnumerable<Guid> GetItemsInWorkflowState(Guid workflowId, Guid workflowStateId)
        {
            return _workflowService.GetItemsInWorkflowState(workflowId, workflowStateId);
        }
    }
}