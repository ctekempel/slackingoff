﻿using System;
using System.Collections.Generic;
using SlackingOff.WorkflowProvider.TriggerWorkflowCommand.Model;

namespace SlackingOff.WorkflowProvider.Services
{
    public interface IWorkflowService
    {
        void TriggerWorkflowCommand(ExecuteWorkflowCommandRequest commandRequest);
        IEnumerable<Guid> GetItemsInWorkflowState(Guid workflowId, Guid workflowStateId);
    }
}