﻿using Sitecore.DataExchange.Contexts;
using Sitecore.DataExchange.Extensions;
using Sitecore.DataExchange.Models;
using Sitecore.DataExchange.Processors.PipelineSteps;
using Sitecore.Diagnostics;
using Sitecore.Services.Core.Diagnostics;
using SlackingOff.WorkflowProvider.Services;
using SlackingOff.WorkflowProvider.TriggerWorkflowCommand.Model;

namespace SlackingOff.WorkflowProvider.TriggerWorkflowCommand
{
    public class TriggerWorkflowCommandStepProcessor : BasePipelineStepWithEndpointsProcessor
    {
        protected override void ProcessPipelineStep(PipelineStep pipelineStep = null, PipelineContext pipelineContext = null,
                ILogger logger = null)
        {
            var endpointSettings = pipelineStep.GetEndpointSettings();
            Assert.IsNotNull(endpointSettings, "Endpoint settings cannot be null");
            var endpoint = endpointSettings.EndpointTo;
            Assert.IsNotNull(endpoint, "Endpoint To must be set");
            //var providerSettings = endpoint.GetWorkflowProviderSettings();

            var commandRequest = GetObjectFromPipelineContext(pipelineStep.GetDataLocationSettings().DataLocation,
                pipelineContext, logger) as ExecuteWorkflowCommandRequest;
            Assert.IsNotNull(commandRequest, "Command Request not found in data location");

            var workflowService = new WorkflowServiceWrapper();
            workflowService.TriggerWorkflowCommand(commandRequest);

        }
    }
}