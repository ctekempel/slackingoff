﻿namespace SlackingOff.WorkflowProvider.TriggerWorkflowCommand.Model
{
    public class ExecuteWorkflowCommandRequest
    {
        public ExecuteWorkflowCommandRequest()
        {
            
        }
        public ExecuteWorkflowCommandRequest(string itemId, string commandName, string comment = null)
        {
            ItemId = itemId;
            CommandName = commandName;
            Comment = comment;
        }

        public string ItemId { get; set; }
        public string CommandName { get; set; }
        public string Comment { get; set; }
    }
}