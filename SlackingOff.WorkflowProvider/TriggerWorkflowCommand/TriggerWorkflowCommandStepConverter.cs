﻿using Sitecore.DataExchange.Attributes;
using Sitecore.DataExchange.Converters.PipelineSteps;
using Sitecore.DataExchange.Models;
using Sitecore.DataExchange.Plugins;
using Sitecore.DataExchange.Repositories;
using Sitecore.Services.Core.Model;

namespace SlackingOff.WorkflowProvider.TriggerWorkflowCommand
{
    [SupportedIds(ExecuteWorkflowCommandRequestTemplateId)]
    public class TriggerWorkflowCommandStepConverter : BasePipelineStepConverter
    {
        public const string ExecuteWorkflowCommandRequestTemplateId = "{8A0971C0-D0B1-400B-A1EE-BFF475B1D6F6}";
        public const string FieldNameEndpointTo = "EndpointTo";
        public const string FieldNameItemLocation = "ItemLocation";

        public TriggerWorkflowCommandStepConverter(IItemModelRepository repository) : base(repository)
        {
        }

        protected override void AddPlugins(ItemModel source, PipelineStep pipelineStep)
        {
            AddEndpointSettings(source, pipelineStep);
            AddDataLocationSettings(source, pipelineStep);
        }

        private void AddDataLocationSettings(ItemModel source, PipelineStep pipelineStep)
        {
            var settings = new DataLocationSettings
            {
                DataLocation = GetGuidValue(source, FieldNameItemLocation)
            };
            pipelineStep.AddPlugin(settings);
        }

        private void AddEndpointSettings(ItemModel source, PipelineStep pipelineStep)
        {
            var settings = new EndpointSettings
            {
                EndpointTo = ConvertReferenceToModel<Sitecore.DataExchange.Models.Endpoint>(source, FieldNameEndpointTo)
            };

            pipelineStep.AddPlugin(settings);
        }
    }
}