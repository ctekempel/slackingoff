﻿using Sitecore.DataExchange.Converters.Endpoints;
using Sitecore.DataExchange.Repositories;
using Sitecore.Services.Core.Model;

namespace SlackingOff.WorkflowProvider.Endpoint
{
    public class WorkflowEndpointConverter : BaseEndpointConverter
    {
        public const string WorkflowEndpointTemplateId = "{E96FE915-3C16-453A-8ED9-7739C2E650EA}";


        public const string WorkflowStateToMonitorFieldName = "WorkflowStateToMonitor";
        public const string WorkflowToMonitorFieldName = "WorkflowToMonitor";

        public WorkflowEndpointConverter(IItemModelRepository repository) : base(repository)
        {
        }

        protected override void AddPlugins(ItemModel source, Sitecore.DataExchange.Models.Endpoint endpoint)
        {
            var settings = new WorkflowProviderSettings
            {
                WorkflowStateToMonitor = GetStringValue(source, WorkflowStateToMonitorFieldName),
                WorkflowToMonitor = GetStringValue(source, WorkflowToMonitorFieldName)
            };
            endpoint.AddPlugin(settings);
        }
    }
}