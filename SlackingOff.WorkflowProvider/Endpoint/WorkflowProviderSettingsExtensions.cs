﻿namespace SlackingOff.WorkflowProvider.Endpoint
{
    public static class WorkflowProviderSettingsExtensions
    {
        public static WorkflowProviderSettings GetWorkflowProviderSettings(this Sitecore.DataExchange.Models.Endpoint endpoint)
        {
            return endpoint.GetPlugin<WorkflowProviderSettings>();
        }
        public static bool HasWorkflowProviderSettings(this Sitecore.DataExchange.Models.Endpoint endpoint)
        {
            return (GetWorkflowProviderSettings(endpoint) != null);
        }
    }
}