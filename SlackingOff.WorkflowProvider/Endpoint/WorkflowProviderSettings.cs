﻿using Sitecore.DataExchange;

namespace SlackingOff.WorkflowProvider.Endpoint
{
    public class
        WorkflowProviderSettings : IPlugin
    {
        public string WorkflowStateToMonitor { get; set; }
        public string WorkflowToMonitor { get; set; }
    }
}